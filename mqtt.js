const { Worker, isMainThread, parentPort } = require('worker_threads');
const mqtt = require('mqtt');
const { MongoClient } = require('mongodb');

if (isMainThread) {
  // Main thread code

  // Create a worker thread to handle MQTT message reception
  const worker = new Worker(__filename);

  // Listen for messages from the worker thread
  worker.on('message', (message) => {
    console.log(`Main thread received message: ${message}`);
  });

  // Send a message to the worker thread
  worker.postMessage('Hello from the main thread!');
} else {
  // Worker thread code

  // MongoDB connection URL
  const mongoURI = 'mongodb://localhost:27017';
  // Database Name
  const dbName = 'mydatabase';
  // Collection Name
  const collectionName = 'mqttData';

  // Create the MongoDB client
  const mongoClient = new MongoClient(mongoURI);

  // Connect to MongoDB




    // Create the MQTT client in the worker thread
    const mqttClient = mqtt.connect('mqtt://pytcp2.transight.in:1883');

    // Subscribe to the desired MQTT topic(s)
    mqttClient.subscribe('transight/#');

    // Handle incoming MQTT messages
    mqttClient.on('message', async (topic, message) => {
      // Process the received message
      console.log(`Worker thread received message on topic ${topic}: ${message}`);

      try {
        // Parse JSON data from the incoming payload
        const data = JSON.parse(message);

        // Get the MongoDB database
        const db = mongoClient.db(dbName);

        // Get the collection
        const collection = db.collection(collectionName);

        // Insert the parsed JSON data into MongoDB
        await collection.insertOne({ topic, data });

        console.log('Data saved to MongoDB');
      } catch (error) {
        console.error('Error processing message : ', error);
      }

      // Send a message back to the main thread
      parentPort.postMessage('Hello from the worker thread ! ');
    });
  
}
