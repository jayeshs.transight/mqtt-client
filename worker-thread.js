const Ajv = require("ajv");
const mqtt = require('mqtt');
const { MongoClient } = require('mongodb');
const dotenv = require('dotenv');

dotenv.config();

const mongoURI = `mongodb://${process.env.dbuser}:${process.env.dbpass}@${process.env.dbhost}:${process.env.dbport}`;

console.log("************************************************************************************");
console.log(mongoURI);
console.log("************************************************************************************");
const dbName = `${process.env.db}`;
const collectionName = `${process.env.collection}`;

const mongoClient = new MongoClient(mongoURI);


const schema1 = {
    type: "object",
    properties: {
        op: { type: "number" },
        to: { type: "string" },
        fr: { type: "string" },
        rqi: { type: "string" },
        rvi: { type: "string" },
        ty: { type: "number" },
        pc: {
            type: "object",
            properties: {
                "hd:wgps": {
                    type: "object",
                    properties: {
                        cnd: { type: "string" },
                        fw: { type: "string" },
                        pkSt: { type: "string" },
                        dgt: { type: "string" }, 
                        
                    },
                    required: ["fw", "pkSt", "dgt", "lat", "latD", "lng", "lngD", "spd", "noS", "hdop", "pdop", "alt", "mcc", "mnc", "lac", "cellId", "rssi", "ber", "btPer", "mvnt", "tmp", "dst", "cnt", "bleV", "bleL", "bleS", "recF", "tftA", "tnpA", "dacT"]
                }
            },
            required: ["hd:wgps"]
        }
    },
    required: ["op", "to", "fr", "rqi", "rvi", "ty", "pc"]
};

const schema2 = {
    type: "object",
    properties: {
        op: { type: "number" },
        to: { type: "string" },
        fr: { type: "string" },
        rqi: { type: "string" },
        rvi: { type: "string" },
        pc: {
            type: "object",
            properties: {
                "hd:wgps": {
                    type: "object",
                    properties: {
                        fw: { type: "string" },
                        pkSt: { type: "string" },
                        
                    },
                    required: ["fw", "pkSt", "dgt", "lat", "latD", "lng", "lngD", "spd", "noS", "hdop", "pdop", "alt", "mcc", "mnc", "lac", "cellId", "rssi", "ber", "btPer", "mvnt", "tmp", "dst", "cnt", "recF"]
                }
            },
            required: ["hd:wgps"]
        }
    },
    required: ["op", "to", "fr", "rqi", "rvi", "pc"]
};

const schema3 = {
    type: "object",
    properties: {
        op: { type: "number" },
        to: { type: "string" },
        fr: { type: "string" },
        rqi: { type: "string" },
        ty: { type: "number" },
        pc: {
            type: "object",
            properties: {
                "m2m:sgn": {
                    type: "object",
                    properties: {
                        nev: {
                            type: "object",
                            properties: {
                                rep: {
                                    type: "object",
                                    properties: {
                                        "m2m:cin": {
                                            type: "object",
                                            properties: {
                                                con: {
                                                    type: "object",
                                                    properties: {
                                                        ctlTy: { type: "string" },
                                                        ctlVal: { type: "string" }
                                                    },
                                                    required: ["ctlTy", "ctlVal"]
                                                }
                                            },
                                            required: ["con"]
                                        }
                                    },
                                    required: ["m2m:cin"]
                                }
                            },
                            required: ["rep"]
                        },
                        net: { type: "number" }
                    },
                    required: ["nev", "net"]
                },
                sur: { type: "string" },
                cr: { type: "string" }
            },
            required: ["m2m:sgn", "sur", "cr"]
        },
        rvi: { type: "string" }
    },
    required: ["op", "to", "fr", "rqi", "ty", "pc", "rvi"]
};

const ajv = new Ajv();


const validatePacket1 = ajv.compile(schema1);
const validatePacket2 = ajv.compile(schema2);
const validatePacket3 = ajv.compile(schema3);

async function connectToMongoDB() {
    try {
        await mongoClient.connect();
        console.log('Connected to MongoDB.');
    } catch (error) {
        console.error('Failed to connect to MongoDB', error);
        process.exit(1); 
    }
}

async function saveMessageToMongoDB(topic, data, serverHitTime, dst, dgt,errors) {
    try {
        const db = mongoClient.db(dbName);
        const collection = db.collection(collectionName);

        
        await collection.insertOne({ topic, data, serverHitTime, dst, dgt,errors });
        console.log('Data saved to MongoDB.');
    } catch (error) {
        console.error('Error saving message to MongoDB:', error);
    }
}

function validateAndSavePacket(topic, message) {
    
    const serverHitTime = new Date();

    try {
        
        const data = JSON.parse(message.toString());

        
        let packetSchema;
        let packetType;

        if (data.op && data.pc && data.pc["hd:wgps"] && data.pc["hd:wgps"].cnd) {
            packetSchema = validatePacket1;
            packetType = 1;
        } else if (data.op && data.pc && data.pc["hd:wgps"]) {
            packetSchema = validatePacket2;
            packetType = 2;
        } else if (data.op && data.pc && data.pc["m2m:sgn"]) {
            packetSchema = validatePacket3;
            packetType = 3;
        } else {
            console.error('Unknown packet type.');
            return;
        }

        
        const valid = packetSchema(data);
        var ers="";
        if (!valid) {
            console.log(`Validation errors for packet ${packetType}: ${JSON.stringify(packetSchema.errors)}`);
            errors=JSON.stringify(packetSchema.errors);
        } else {
            console.log(`Packet ${packetType} is valid.`);
        }

        // Save the message along with its topic and server hit time to MongoDB
        saveMessageToMongoDB(topic, data, serverHitTime, data.pc["hd:wgps"].dst, data.pc["hd:wgps"].dgt,errors);

        // Extract and log the 'dgt' and 'dst' fields
        if (data.pc && data.pc["hd:wgps"] && data.pc["hd:wgps"].dgt) {
            console.log(`'dgt' field value: ${data.pc["hd:wgps"].dgt}`);
        }

        if (data.pc && data.pc["hd:wgps"] && data.pc["hd:wgps"].dst) {
            console.log(`'dst' field value: ${data.pc["hd:wgps"].dst}`);
        }
    } catch (error) {
        console.error('Error processing message:', error);
    }
}

function setupMQTTClient() {
    
    const mqttClient = mqtt.connect('mqtt://pytcp2.transight.in:1883');

    
    mqttClient.on('connect', () => {
        mqttClient.subscribe('#', (err) => {
            if (err) {
                console.error('Failed to subscribe:', err);
            } else {
                console.log('MQTT subscription successful.');
            }
        });
    });

   
    mqttClient.on('message', (topic, message) => {
        console.log(`Received message on topic ${topic}: ${message}`);

        
        validateAndSavePacket(topic, message);
    });
}

async function main() {
    await connectToMongoDB(); 
    setupMQTTClient(); 
}

main().catch(console.error);
