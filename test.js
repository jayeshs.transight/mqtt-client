const Ajv = require("ajv");

// Define your JSON schema
const schema = {
  type: "object",
  properties: {
    op: { type: "number" },
    to: { type: "string" },
    fr: { type: "string" },
    rqi: { type: "string" },
    ty: { type: "number" },
    pc: {
     type: "object",
      properties: {
        "m2m:sgn": {
          type: "object",
          properties: {
            nev: {
              type: "object",
              properties: {
                rep: {
                  type: "object",
                  properties: {
                    "m2m:cin": {
                      type: "object",
                      properties: {
                        con: {
                          type: "object",
                          properties: {
                            ctlTy: { type: "string" },
                            ctlVal: { type: "string" }
                          },
                          required: ["ctlTy", "ctlVal"]
                        }
                      },
                      required: ["con"]
                    }
                  },
                  required: ["m2m:cin"]
                }
              },
              required: ["rep"]
            },
            net: { type: "number" }
          },
          required: ["nev", "net"]
        },
        sur: { type: "string" },
        cr: { type: "string" }
      },
      required: ["m2m:sgn", "sur", "cr"]
    },
    rvi: { type: "string" }
  },
  required: ["op", "to", "fr", "rqi", "ty", "pc", "rvi"]
};

// Create an Ajv instance
const ajv = new Ajv();

// Compile the schema
const validate = ajv.compile(schema);

// Define your JSON data
const jsonData = {
  "op": 5,
  "to": "<registrarURL>/oneM2M/req/<registrarCseId>/<Device-AE-ID>/json",
  "fr": "/<registrarCseId>",
  "rqi": "<unique-Request-Identifier>",
  "ty": 4,
  "pc": {
    "m2m:sgn": {
      "nev": {
        "rep": {
          "m2m:cin": {
            "con": {
              "ctlTy": "<String value>",
              "ctlVal": "<String value>"
            }
          }
        }
      },
      "net": 3
    },
    "sur": "/CSE001/R2524",
    "cr": "Svendorcode1"
  },
  "rvi": "3"
};

// Validate the JSON data against the schema
const valid = validate(jsonData);

if (!valid) {
  console.log("Validation errors:", validate.errors);
} else {
  console.log("JSON data is valid.");
}



const searchInput = document.getElementById('searchInput');
const fromDateInput = document.getElementById('fromDateInput');
const toDateInput = document.getElementById('toDateInput');
searchInput.addEventListener('input', filterTable);
fromDateInput.addEventListener('input', filterTable);
toDateInput.addEventListener('input', filterTable);

// Function to filter table based on search query and dates
function filterTable() {
  const query = searchInput.value.toLowerCase();
  const fromDate = fromDateInput.value;
  const toDate = toDateInput.value;
  rows.forEach(row => {
    const cells = Array.from(row.querySelectorAll('td'));
    const match = cells.some(cell => cell.textContent.toLowerCase().includes(query));
    const dateMatch = (fromDate === '' || toDate === '') || (cell.textContent.includes(fromDate) && cell.textContent.includes(toDate));
    row.style.display = match && dateMatch ? '' : 'none';
  });
}
})
.catch(error => console.error('Error fetching data:', error));
