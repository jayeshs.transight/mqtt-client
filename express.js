const express = require('express');
const cors = require('cors');
const { MongoClient } = require('mongodb');
const moment = require('moment-timezone');
require('dotenv').config();

const app = express();
app.use(cors()); 

const uri = `mongodb://${process.env.dbuser}:${process.env.dbpass}@${process.env.dbhost}:${process.env.dbport}`;
const dbName = `${process.env.db}`;
const client = new MongoClient(uri);

app.use(express.json());

app.get('/search', async (req, res) => {
    await client.connect();
    console.log('Connected to MongoDB');
    const db = client.db(dbName);
    const collection = db.collection(`${process.env.collection}`);
    console.log(req.query.searchvalue);
console.log(req.query.fromdate);
console.log(req.query.todate);
const query = {
    $and: [
        {
            dst: {
                $gte: req.query.fromdate,
                $lte: req.query.todate
            }
        },
        {
            topic: { $regex: "^"+req.query.searchvalue+"$", $options: "i" }
        }
    ]
};
    const data = await collection.find(query).toArray();
    console.log(data);
    //console.log(data[0]['serverHitTime']);
    for (let i = 0; i < data.length; i++) {
        console.log("looping");
        const utcTimestamp = data[i].serverHitTime;
        const istTimestamp = moment.tz(utcTimestamp, "Asia/Kolkata").format();
        console.log(istTimestamp);
        data[i].serverHitTime = istTimestamp; // Add the converted time to a new property
    }
    res.json(data);


});

app.get('/data', async (req, res) => {
    try {
        await client.connect();
        console.log('Connected to MongoDB');

        const db = client.db(dbName);
        const collection = db.collection('mqttData');
        let query = {};

     
        const data = await collection.find(query).toArray();
        //console.log(data[0]['serverHitTime']);
        res.json(data);
    } catch (error) {
        console.error('Error retrieving data:', error);
        res.status(500).json({ error: 'Internal Server Error' });
    } finally {
        await client.close();
        console.log('Disconnected from MongoDB');
    }
});

const PORT = process.env.PORT || 5500;
app.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}`);
});
